# Personal Website

Welcome to the repository for [alexgd.me](https://alexgd.me), my personal website. This site is built using [Hugo](https://gohugo.io/), a static site generator known for its speed and flexibility, and utilises the [Gokarna](https://themes.gohugo.io/themes/gokarna/) theme which is elegant and responsive.

## Project Licence

This project is open source and available under the [European Union Public Licence 1.2 (EUPL-1.2)](https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12).

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Before you begin, ensure you have the following installed:
- [Git](https://git-scm.com/)
- [Hugo](https://gohugo.io/getting-started/installing/)

### Installing

To set up a local copy of the site, follow these steps:

1. Clone the repository:
   ```bash
   git clone https://gitlab.com/agdell/alexgd.me.git
   ```
2. Navigate to the project directory
   ```bash
   cd alexgd.me
   ```
3. Run Hugo server:
   ```bash
   hugo server
   ```

   This command will start the Hugo server locally and output the address where you can view the site (typically <http://localhost:1313>).

## Contributing

Contributions are what make the open-source community such an amazing place to learn, inspire, and create. Any contributions you make are greatly appreciated.

1. Fork the Project
2. Create your Feature Branch (git checkout -b feature/AmazingFeature)
3. Commit your Changes (git commit -m 'Add some AmazingFeature')
4. Push to the Branch (git push origin feature/AmazingFeature)
5. Open a Pull Request

## Acknowledgements

- Thanks to the creators and maintainers of Hugo and the Gokarna theme.
- Thanks to everyone who contributes to open source projects.
