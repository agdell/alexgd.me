---
title: Copyright Statement
date: 2024-05-05
description: Content licensed under EUPL-1.2; governed by Belgian law; use subject to terms of licence.
type: page
showTableOfContents: true
---

# Content Licensing

All content available on this website is provided under the European Union Public Licence v. 1.2 (**EUPL-1.2**), which is governed by the laws of Belgium. The `EUPL` permits use, reproduction, modification, distribution, and communication of the licensed content, among other rights, as detailed in the licence terms. For more information on the **EUPL-1.2**, please refer to the [EUPL-1.2 Licence text][EUPL Joinup].

# Copyright Ownership

All content on this website, unless otherwise stated, is the intellectual property of Alex Cedar Garth-Dòmhnallach. This includes all textual content, images, designs, logos, and other materials.

# Use of Content

Users are free to:

- Use the content in any circumstance for all usage.
- Reproduce the content.
- Modify the content and create derivative works.
- Distribute copies of the content.
- Communicate the content to the public.

# Requirements and Restrictions

When using the content from this website, you must:

- Ensure all copyright, patent, or trademarks notices remain intact.
- Include a copy of these notices and a copy of the **EUPL-1.2** Licence with every copy of the content distributed or communicated.
- Ensure any derivative works carry prominent notices stating that the content has been modified and the date of modification.

# Limitation of Liability

The content is provided "as is", and Alex Cedar Garth-Dòmhnallach makes no warranties regarding the content, including but not limited to the warranties of non-infringement. Alex Cedar Garth-Dòmhnallach shall not be liable for any damages resulting from the use of the content as permitted under the terms of the **EUPL-1.2** Licence.

# Governing Law

This website is operated from outside the European Union; however, in compliance with the **EUPL-1.2**, the governing law for any disputes arising from the use of this website is Belgian Law.

# Contact Information

For any questions regarding the use of the content or the terms of the **EUPL-1.2** Licence, please contact me by email at <contact@alexgd.me> or on X (Twitter) at [@agdelloch].

[@agdelloch]: https://twitter.com/agdelloch 
[EUPL Joinup]: https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
