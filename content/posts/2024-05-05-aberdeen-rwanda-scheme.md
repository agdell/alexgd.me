---
title: Standing Against Injustice – Aberdeen Protest against Rwanda Scheme
date: 2024-05-05
description: The alarm was raised late on Wednesday of a potential detainment under the Rwanda Scheme
image: /images/35f5986b-302e-4467-9877-be15120ece47.jpeg
tags: ["politics", "rwanda-scheme", "snp"]
type: post
showTableOfContents: true
---

# Overview
On the 3rd of May 2024, Aberdeen joined the nationwide outcry against the controversial deportation arrangements with Rwanda, hosting a protest outside Marischal College. The demonstration aimed not only to protest against Police Scotland and the Home Office but also to ensure that individuals attending their routine appointments with the Home Office have proper legal representation and safeguarding, should they be detained.

# Context of the Protest
The UK's new policy, part of the Safety of Rwanda Act passed on 23 April 2024, allows for the deportation of certain asylum seekers to Rwanda, where their claims will be processed far from the UK shores. This legislation is part of an international agreement termed the UK-Rwanda Asylum Partnership, signed in December 2023.

# Why We Protest
The Aberdeen protest is part of a broader movement that has seen similar gatherings across the UK. These protests focus on the potential dangers of this policy, which, according to the UN High Commissioner for Refugees (UNHCR), undermines international refugee law and could endanger the lives of the transferred asylum seekers.

# Key Messages
During the protest, the emphasis was on educating attendees about their legal rights. One prominent activist addressed the crowd, saying, “We want people to understand their rights,” highlighting the importance of legal awareness amidst these troubling developments.

# Immediate Threats
Recent updates have indicated a concerning trend: the Home Office may increase detentions from late April through mid-May 2024, targeting individuals primarily during routine appointments[^1]. This approach creates an urgent need for legal preparedness and public awareness to prevent unlawful detentions without due process.

# What Can You Do?
For those at risk, the guidance is straightforward:
- **Do not sign any documents** without consulting your lawyer.
- **Inform a trusted contact** about the time and place of your appointment.
- **Keep legal and emergency contact details handy** in case you are detained.

# Closing Thoughts
The protest at Marischal College was a powerful reminder of the collective strength and resolve of our community. We stood together not just to voice our opposition but to act as a beacon of support for those who are most vulnerable under these new policies.

We continue to advocate for fairness, transparency, and the protection of human rights, ensuring that those affected are not alone in this fight.

[^1]: This document was being circulated by No Recourse, North East Partnership ([@RecourseNo](https://twitter.com/RecourseNo)): [Rwanda Information Sheet](https://docs.google.com/document/d/1hYBat1s1qyLSq0TaDRsZuyLOW5sO8fKnJhSeMZqEYAY/edit?usp=drivesdk)
