---
title: {{ replace .File.ContentBaseName "-" " " | title }}
date: {{ .Date }}
description: "A blog post"
image: "/path/to/image.png"
type: "post"
tags: ["blog"]
draft: true
---

# Hello World!
This is my blog.